cmake_minimum_required( VERSION 3.14 )

project( "pg-variant-op" VERSION 0.1.2
        DESCRIPTION "PostgreSQL variant hive operation as composite type"
        LANGUAGES CXX )

add_subdirectory( vendor )

option( PVO_BUILD_TESTS "Optionally builds tests and their dependencies (ON/OFF)" OFF )
option( PG_PROVE_ARGS "Optional pg_prove arguments for the ctest (\"\")" "" )

if( PVO_BUILD_TESTS )
        add_subdirectory( tests )
endif()
