BEGIN;
SELECT plan( 2 );

DELETE FROM hive.operations WHERE id = -1;

DROP FUNCTION IF EXISTS hive._pgtap__get_test_op_body;

DROP DOMAIN IF EXISTS hive._pgtap__op_type;
CREATE DOMAIN hive._pgtap__op_type AS hive.limit_order_cancel_operation;

CREATE OR REPLACE FUNCTION hive._pgtap__get_test_op_body (
) RETURNS hive._pgtap__op_type LANGUAGE plpgsql AS $$
BEGIN
	RETURN (
    'initminer',
    123456
  ) :: hive._pgtap__op_type;
END
$$;

SELECT lives_ok($$ INSERT INTO hive.operations VALUES( -1, hive._pgtap__get_test_op_body() ); $$);

SELECT results_eq($$
  SELECT body :: hive._pgtap__op_type FROM hive.operations WHERE id = -1;
$$, $$
  SELECT hive._pgtap__get_test_op_body() AS body;
$$);

SELECT * FROM finish();
ROLLBACK;
