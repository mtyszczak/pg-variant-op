BEGIN;
SELECT plan( 2 );

DELETE FROM hive.operations WHERE id = -1;

DROP FUNCTION IF EXISTS hive._pgtap__get_test_op_body;

DROP DOMAIN IF EXISTS hive._pgtap__op_type;
CREATE DOMAIN hive._pgtap__op_type AS hive.report_over_production_operation;

CREATE OR REPLACE FUNCTION hive._pgtap__get_test_op_body (
) RETURNS hive._pgtap__op_type LANGUAGE plpgsql AS $$
BEGIN
	RETURN (
    'initminer',
    (
      'abc \153\154\155 \052\251\124'::bytea,
      to_timestamp(1657886419),
      'initminer',
      'abc \153\154\155 \052\251\124'::bytea,
      ARRAY[
        ROW() :: hive.void_t,
        ROW(123456) :: hive.version,
        (ROW(123456) :: hive.version, to_timestamp(1657886419)) :: hive.hardfork_version_vote,
        ARRAY[ROW('initminer') :: hive.example_required_action] :: hive.required_automated_actions :: hive.hive_block_header_extension,
        ARRAY[ROW('initminer') :: hive.example_optional_action] :: hive.optional_automated_actions :: hive.hive_block_header_extension
      ] :: hive.block_header_extensions,
      'abc \153\154\155 \052\251\124'::bytea
    ) :: hive.signed_block_header,
    (
      'abc \153\154\155 \052\251\124'::bytea,
      to_timestamp(1657886419),
      'initminer',
      'abc \153\154\155 \052\251\124'::bytea,
      ARRAY[
        ROW() :: hive.void_t,
        ROW(123456) :: hive.version,
        (ROW(123456) :: hive.version, to_timestamp(1657886419)) :: hive.hardfork_version_vote,
        ARRAY[ROW('initminer') :: hive.example_required_action] :: hive.required_automated_actions :: hive.hive_block_header_extension,
        ARRAY[ROW('initminer') :: hive.example_optional_action] :: hive.optional_automated_actions :: hive.hive_block_header_extension
      ] :: hive.block_header_extensions,
      'abc \153\154\155 \052\251\124'::bytea
    ) :: hive.signed_block_header
  ) :: hive._pgtap__op_type;
END
$$;

SELECT lives_ok($$ INSERT INTO hive.operations VALUES( -1, hive._pgtap__get_test_op_body() ); $$);

SELECT results_eq($$
  SELECT body :: hive._pgtap__op_type FROM hive.operations WHERE id = -1;
$$, $$
  SELECT hive._pgtap__get_test_op_body() AS body;
$$);

SELECT * FROM finish();
ROLLBACK;
