BEGIN;
SELECT plan( 2 );

DELETE FROM hive.operations WHERE id = -1;

DROP FUNCTION IF EXISTS hive._pgtap__get_test_op_body;

DROP DOMAIN IF EXISTS hive._pgtap__op_type;
CREATE DOMAIN hive._pgtap__op_type AS hive.create_claimed_account_operation;

CREATE OR REPLACE FUNCTION hive._pgtap__get_test_op_body (
) RETURNS hive._pgtap__op_type LANGUAGE plpgsql AS $$
BEGIN
	RETURN (
    'initminer',
    'initminer2',
    ( 1, ARRAY[( 'initminer', '1' ) :: hive._account_auths_authority],
      ARRAY[( 'STM8GC13uCZbP44HzMLV6zPZGwVQ8Nt4Kji8PapsPiNq1BK153XTX', '1' ) :: hive._key_auths_authority] ),
    ( 2, ARRAY[( 'initminer', '2' ) :: hive._account_auths_authority],
      ARRAY[( 'STM8GC13uCZbP44HzMLV6zPZGwVQ8Nt4Kji8PapsPiNq1BK153XTX', '2' ) :: hive._key_auths_authority] ),
    ( 3, ARRAY[( 'initminer', '3' ) :: hive._account_auths_authority],
      ARRAY[( 'STM8GC13uCZbP44HzMLV6zPZGwVQ8Nt4Kji8PapsPiNq1BK153XTX', '3' ) :: hive._key_auths_authority] ),
    'STM8GC13uCZbP44HzMLV6zPZGwVQ8Nt4Kji8PapsPiNq1BK153XTX',
    '\{\}',
    ARRAY[ ROW() :: hive.void_t, ROW() :: hive.void_t ]
  ) :: hive._pgtap__op_type;
END
$$;

SELECT lives_ok($$ INSERT INTO hive.operations VALUES( -1, hive._pgtap__get_test_op_body() ); $$);

SELECT results_eq($$
  SELECT body :: hive._pgtap__op_type FROM hive.operations WHERE id = -1;
$$, $$
  SELECT hive._pgtap__get_test_op_body() AS body;
$$);

SELECT * FROM finish();
ROLLBACK;
