BEGIN;
SELECT plan( 2 );

DELETE FROM hive.operations WHERE id = -1;

DROP FUNCTION IF EXISTS hive._pgtap__get_test_op_body;

DROP DOMAIN IF EXISTS hive._pgtap__op_type;
CREATE DOMAIN hive._pgtap__op_type AS hive.comment_options_operation;

CREATE OR REPLACE FUNCTION hive._pgtap__get_test_op_body (
) RETURNS hive._pgtap__op_type LANGUAGE plpgsql AS $$
BEGIN
	RETURN (
    'initminer',
    'https://hive.blog/initminer/hellow-world',
    ( 0, 1 ),
    200,
    true,
    true,
    ARRAY[
      ROW( ARRAY[(0, (1, true) :: hive.votable_asset_info_v1) :: hive._votable_asset_allowed_vote_assets] ) :: hive.allowed_vote_assets,
	  ROW( ARRAY[('initminer', 1) :: hive.beneficiary_route_type, ('initminer2', 2) :: hive.beneficiary_route_type] ) :: hive.comment_payout_beneficiaries
    ] :: hive.comment_options_extensions_type
  ) :: hive._pgtap__op_type;
END
$$;

SELECT lives_ok($$ INSERT INTO hive.operations VALUES( -1, hive._pgtap__get_test_op_body() ); $$);

SELECT results_eq($$
  SELECT body :: hive._pgtap__op_type FROM hive.operations WHERE id = -1;
$$, $$
  SELECT hive._pgtap__get_test_op_body() AS body;
$$);

SELECT * FROM finish();
ROLLBACK;
