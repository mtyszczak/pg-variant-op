CREATE TABLE IF NOT EXISTS hive.operations (
    id bigint not null,
    body hive_operation,
    CONSTRAINT pk_hive_operations PRIMARY KEY ( id )
);
