CREATE TYPE hive.curve_id AS ENUM (
  'quadratic',
  'bounded_curation',
  'linear',
  'square_root',
  'convergent_linear',
  'convergent_square_root'
);
