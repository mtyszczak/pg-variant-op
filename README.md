# pg-admin-op

PostgreSQL variant [Hive](https://gitlab.syncad.com/hive/hive) operation as composite type

## Building

### Building on Ubuntu 22.04

```bash
# install dependencies
sudo apt update

sudo apt install -y \
    git \
    make \
    postgresql-server-dev-all # Version 14 is confirmed compatible

git clone https://gitlab.com/mtyszczak/pg-variant-op.git
cd pg-variant-op
git checkout main

mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
sudo make -j$(nproc)

# Optional
sudo make install # install defaults to /usr/local
```

This shall build all of the required extensions

## Example usage

```sql
CREATE EXTENSION "variant"; -- Install the variant extension

SELECT 42::int::variant.variant; -- Check if loaded correctly. Should result in the following output:
/*
   variant
--------------
 (integer,42)
(1 row)
*/

CREATE EXTENSION "hive_fork_manager"; -- Install the haf hive_fork_manager extension

INSERT INTO hive.operations VALUES( 1, ( 'initminer', 'initminer', 'https://hive.blog/', 1 ) :: hive.vote_operation );
-- Check insertion (cast from arbitrary type to a variant). Should result in the following output:
/*
INSERT 0 1
*/

SELECT ( SELECT body :: hive.vote_operation from hive.operations LIMIT 1 ).voter;
-- Check selection (cast from variant to an arbitrary type). Should result in the following output:
/*
   voter
-----------
 initminer
(1 row)
*/
```

## Required HAF changes

Changes required to be done to the [HAF codebase](https://gitlab.syncad.com/hive/haf) before applying my code:

1. Change `hive.operations.body` type from `string NOT NULL` to `hive_operation`

2. Add `variant` to the `hive_fork_manager` control file `requires`

## Testing

Install Perl module:

```bash
cpan TAP::Parser::SourceHandler::pgTAP
```

Run cmake again, but with `-DPVO_BUILD_TESTS=ON` option

Run make again

Run tests:

```bash
cd tests

ctest --stop-on-failure --output-on-failure
```

Note: If you want to customize `pg_prove` options you can put them in the cmake `PG_PROVE_ARGS` option, e.g. `-DPG_PROVE_ARGS="-Upostgres"`

Also, if you do not want to paste password every time the connection is established, you create the `~/.pgpass` with `0600` permissions and data in format:
`hostname:port:database:username:password`

## License

Distributed under the GNU GENERAL PUBLIC LICENSE Version 3
See [LICENSE.md](LICENSE.md)
